train_path = 'data/train_data.csv'
test_path = 'data/test_data.csv'

# BERT

pretrained_bert_path = 'models/rubert_cased_L-12_H-768_A-12_pt_v1'
batch_size = 16
learning_rate = 2e-5
eps = 1e-8
num_epochs = 5
seed_val = 42